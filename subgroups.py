# Subgroup Discovery for COVID-19 patients
# M.Y. Kingma
# Student Master Medical Informatics
# Amsterdam UMC, Universiteit van Amsterdam

#%% Import libraries
from cmath import log
import pandas as pd
import pyarrow.feather as pf
import pysubgroup as ps
from PRIM.src import PRIM
import numpy as np

#### Script settings ####
# Filename data file, move this file to the same directory as the script itself
filename = 'data.feather'

# Amount of patients taken from the dataset used for subgroup discovery
patients = 400

# Amount of variables used for subgroup discovery
variables = 64 # Max 64

# Select the desired amount of subgroups (only supported by APRIORI and BEAM algorithms from PySubgroup)
subgroup_amount = 5

# Select maximum amount of rules/conditions per group (only supported by APRIORI and BEAM algorithms from PySubgroup)
selector_depth = 3


#### Global functions ####
def create_subgroup_specific_results(index, rules, WRAcc, size_sg, pos_sg, group_df):
    '''
    Create result dictionary with subgroup specific results

    param index: index of subgroup
    param rules: rule representation of subgroup
    param WRAcc: Weighted Relatice Accuracy statistic of subgroup
    param size_sg: size of subgroup
    param pos_sg: amount of positive target variables in subgroup
    param group_df: dataframe of subgroup

    returns: subgroup result dictionary with quality measure statistics
    '''

    # Declare dict for subgroup
    group = {}

    # Set group specific results
    group['index'] = index
    group['rules'] = rules
    group['WRAcc'] = WRAcc
    group['size'] = size_sg
    group['size_dataset'] = instances_dataset
    group['positives_sg'] = pos_sg
    group['positives_dataset'] = positives_dataset
    group['coverage'] = size_sg / instances_dataset
    group['support'] = group['positives_sg'] / instances_dataset
    group['confidence'] = group['positives_sg'] / size_sg
    negatives_subgroup = size_sg - pos_sg
    group['significance'] = 2 * (
        pos_sg * log(pos_sg / (positives_dataset * (size_sg / instances_dataset))) +
        negatives_subgroup * log(negatives_subgroup / (negatives_dataset * (size_sg / instances_dataset)))
    )

    # Add dataframe of subgroup
    group['df'] = group_df

    return group

def analize_overall_results(subgroups):
    '''
    Analyse overall results from specific algorithm subgroup set

    param subgroups: subgroup discovery result dictionary

    returns: result dictionary with array of subgroups and dict with overall quality measures
    '''

    dfs = []
    for group in subgroups['subgroups']:
        dfs.append(group['df'])

    all_covered = pd.concat(dfs)
    all_covered = all_covered.drop_duplicates()
    all_positive = all_covered[all_covered['dood'] == True].shape[0]
    subgroups['results']['overall_coverage'] = all_covered.shape[0] / instances_dataset
    subgroups['results']['overall_support'] = all_positive / instances_dataset

    total_subgroups = len(subgroups['subgroups'])
    total_coverage = 0
    total_support = 0
    total_WRAcc = 0
    total_confidence = 0
    total_significance = 0
    max_WRAcc = 0

    for group in subgroups['subgroups']:
        total_coverage += group['coverage']
        total_support += group['support']
        total_WRAcc += group['WRAcc']
        total_confidence += group['confidence']
        total_significance += group['significance']
        if group['WRAcc'] > max_WRAcc:
            max_WRAcc = group['WRAcc']

    subgroups['results']['average_coverage'] = total_coverage / total_subgroups
    subgroups['results']['average_support'] = total_support / total_subgroups
    subgroups['results']['average_WRAcc'] = total_WRAcc / total_subgroups
    subgroups['results']['average_confidence'] = total_confidence / total_subgroups
    subgroups['results']['average_significance'] = total_significance / total_subgroups
    subgroups['results']['max_WRAcc'] = max_WRAcc

#### Data load ####
## Constants
# Declaration of all categorical and numeric columns (not boolean)
not_boolean_columns = ['finale_covid_status', 'nice_age', 'gender', 'verschillende_ics', 'ref_spec', 'adm_source', 'adm_type', 'heartrate_min', 'heartrate_max', 'resprate_min', 'resprate_max', 'syst_min', 'syst_max', 'meanbl_min', 'meanbl_max', 'temp_min', 'temp_max', 'urine_24', 'paco2', 'fio2', 'nice_pao2_fio2', 'pao2', 'ph_min', 'wbc_min', 'wbc_max', 'creat_min', 'creat_max', 'potas_min', 'potas_max', 'sodium_min', 'sodium_max', 'bicarb_min', 'bicarb_max', 'urea', 'ht_min', 'ht_max', 'throm_min', 'gluc_min', 'gluc_max', 'nice_a_ado2', 'nice_gcs_low', 'los_pre', 'bmi', 'aantalchronisch', 'golf']

# Import data file 
df = pf.read_feather(filename)

# Remove empty values
df = df.dropna()

# Convert 0/1 columns to boolean columns
for column in df:
    if column not in not_boolean_columns:
        df[column] = df[column].map({'0': False, '1': True})
        df[column] = df[column].astype('bool')

# Convert int32 columns to float64 columns
df = df.astype({"verschillende_ics": "float64", "nice_age": "float64"})

# Create subset of data according to settings
sub_df = df.iloc[0:patients, 0:variables]

# Add target variable (dood/death)
sub_df = sub_df.assign(dood = df[0:patients]["dood"])

## Subset constants
# Amount of patients in subset
instances_dataset = patients

# Amount of patients with positive (or negative) target variable
positives_dataset = sub_df[sub_df['dood'] == True].shape[0]
negatives_dataset = instances_dataset - positives_dataset

# Probability of positive target variable in subset
p_dataset = positives_dataset / instances_dataset


#### PySubgroup package ####
# Declare target variable and searchspace
target = ps.BinaryTarget ('dood', True)
searchspace = ps.create_selectors(sub_df, ignore=['dood'])

# Declare task
task = ps.SubgroupDiscoveryTask (
    sub_df,
    target,
    searchspace,
    result_set_size=subgroup_amount,
    depth=selector_depth,
    qf=ps.WRAccQF()) # Quality measure subgroups

# Ignore error created by PySubgroup in true_divide function
np.seterr(divide='ignore', invalid='ignore')

# Execute task with BEAM algorithm
resultDataBeam = ps.BeamSearch().execute(task)

# Execute task with APRIORI algorithm
resultDataApriori = ps.Apriori().execute(task)

# Go to origional error settings
np.seterr(divide='raise', invalid='raise')

## Result analysis
def analyse_subgroups_pysubgroup(resultObject, data):
    '''
    Analyse results from PySubgroup package

    param resultObject: SubgroupDiscoveryResult object returned from task execution
    param data: dataset, or subset used by task execution

    returns: result dictionary with array of subgroups and dict with overall quality measures
    '''

    # Declare index and result dictionary
    index = 0
    subgroups = {'subgroups': [], 'results': {}}

    # Loop over subgroups in resultObject
    for (q, sg, stats) in resultObject.results:

        # Calculate subgroup size and cover array
        # Cover array: true/false array mapping if patient is part of subgroup,
        # for complete dataset used in the subgroup discovery
        if hasattr(sg, "representation"):
            cover_arr = sg.representation
            size_sg = sg.representation.sum()
        else:
            cover_arr, size_sg = ps.get_cover_array_and_size(sg, len(data), data)

        # Add cover array to dataset
        data = data.assign(in_subgroup = cover_arr).copy()

        # Create subset containing subgroup patiens following cover array
        group_df = data[data['in_subgroup'] == True].copy()

        # Remove in_subgroup column from group dataframe and dataset
        group_df.drop('in_subgroup', axis=1, inplace=True)
        data.drop('in_subgroup', axis=1, inplace=True)

        # Calculate amount of positive target variable in subgroup
        positives_subgroup = group_df[group_df['dood'] == True].shape[0]

        # Create subgroup specific results
        group = create_subgroup_specific_results(index, sg, q, size_sg, positives_subgroup, group_df)

        # Increment index and append group to result dictionary
        index += 1
        subgroups['subgroups'].append(group)
    
    return subgroups

# Call analyse function for BEAM and APRIORI algorithms
subgroupsBEAM = analyse_subgroups_pysubgroup(resultDataBeam, sub_df)
subgroupsAPRIORI = analyse_subgroups_pysubgroup(resultDataApriori, sub_df)


#### PRIM module ####
''' Using PRIM module cloned from https://github.com/martinsps/PRIM '''

# Change boolean columns to category columns for PRIM module
for column in sub_df:
    if column not in not_boolean_columns:
        sub_df[column] = sub_df[column].astype('category')

# Create PRIM instance
p = PRIM.PRIM(sub_df, 'dood', True, ordinal_columns={}, )

# Execute algorithm
p.execute()

## Result analysis
# Declare index and result dictionary
subgroupsPRIM = { 'subgroups': [], 'results': {}}
index = 0

# Loop over subgroups (boxes) in PRIM instance
for box in p.boxes:

    # Create subset containing subgroup patiens
    subgroup = p.apply_box(box, sub_df)

    # Calculate subgroup size
    instances_subgroup = subgroup.shape[0]

    # Calculate amount of positive target variable in subgroup
    positives_subgroup = subgroup[subgroup['dood'] == True].shape[0]

    # Probability of positive target variable in subgroup
    p_subgroup = np.divide(positives_subgroup, instances_subgroup)

    # Calculate Weighted Relative Accuracy statistic
    WRAccQF = (instances_subgroup / instances_dataset) ** 1 * (p_subgroup - p_dataset)

    # Create empty rule string
    ruleString = ""

    # Loop over rules for subgroup (box)
    for rule in box.boundary_list:

        # Create rule representation string
        boxRule = rule.__str__()

        # Change syntax to make rule more readable
        boxRule.replace(' != True', '==False').replace(' != False', '==True')
        
        # Append rule to rule string
        if len(ruleString):
            ruleString += " AND "
        ruleString += boxRule

    # Create subgroup specific results
    group = create_subgroup_specific_results(index, ruleString, WRAccQF, instances_subgroup, positives_subgroup, subgroup)

    # Increment index and append group to result dictionary
    index += 1
    subgroupsPRIM['subgroups'].append(group)


#### Overall statistics ####
# Constants
resultObjects = [subgroupsPRIM, subgroupsBEAM, subgroupsAPRIORI]
methods = ['PRIM', 'BEAM', 'APRIORI']

# Loop over resultObjects
for object in resultObjects:
    # Define overall statistics
    analize_overall_results(object)

# Define statistics table outline
statistics_table = {'Method': [], 'coverage_avg': [], 'coverage_overall': [], 'support_avg': [], 'support_overall': [], 'significance_avg': [], 'WRAcc_avg': [], 'WRAcc_max': [], 'confidence_avg': []}

# Fill table with results
for method, result in zip(methods, resultObjects):
    statistics_table['Method'].append(method)
    statistics_table['coverage_avg'].append(result['results']['average_coverage'])
    statistics_table['coverage_overall'].append(result['results']['overall_coverage'])
    statistics_table['support_avg'].append(result['results']['average_support'])
    statistics_table['support_overall'].append(result['results']['overall_support'])
    statistics_table['significance_avg'].append(result['results']['average_coverage'])
    statistics_table['WRAcc_avg'].append(result['results']['average_WRAcc'])
    statistics_table['WRAcc_max'].append(result['results']['max_WRAcc'])
    statistics_table['confidence_avg'].append(result['results']['average_confidence'])

# Convert to DataFrame
statistics_table = pd.DataFrame(statistics_table)